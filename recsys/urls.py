from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('rating', views.rating, name='rating'),
    path('f', views.formList, name='formList'),
    path('getrec', views.getRec, name='getRec'),
]
