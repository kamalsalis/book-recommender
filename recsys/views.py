from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from recsys.tests import *


def index(request):
    context = {
        'details': details
    }
    return render(request, 'recsys/index.html', context)


finalRatings = np.zeros(numofbooks)
ratings = []
bookIDs = []
isbnIDs = list(range(0, numofbooks))


@csrf_exempt
def rating(request):
    bookid = request.POST.get('bookid', None)
    rating = request.POST.get('rating', None)
    bookIDs.append(bookid)
    ratings.append(rating)
    return HttpResponse()


def formList(x, y):
    for a, b in enumerate(x):
        finalRatings[int(b) - 1] = y[a]
    return finalRatings


@csrf_exempt
def getRec(request):
    d = int(request.POST.get('d'))
    userID = []
    userRatings = formList(bookIDs, ratings)
    userRatings = [x * 2 for x in userRatings]
    userID = [x + (len(new_ratings.userID.unique()) + 1) for x in np.zeros(numofbooks)]
    userDict = {'userID': userID, 'ISBN': isbnIDs, 'bookRating': list(userRatings)}
    userRow = pd.DataFrame(userDict)
    df = newer_rating.append(userRow, ignore_index=True, sort=False)
    df = df.drop(columns='Count')
    print(df)
    similarities, indices, prediction = printHello(df, d)
    rectitle = []
    recimage = []
    for x in indices:
        rectitle.append(bookTitle[x])
        recimage.append(bookImage[x])
    recdetails = dict(zip(rectitle, recimage))
    item = bookTitle[d - 1]
    context = {
        'sim': similarities,
        'ind': indices,
        'prediction': prediction,
        'item': item,
        'details': details,
        'recdetails': recdetails
    }
    return render(request, 'recsys/index.html', context)
