import pandas as pd
from django.test import TestCase
import numpy as np

# declaring k,metric as global which can be changed by the user later
global k, numofbooks
k = 5
numofbooks = 25

books = pd.read_csv('recsys/csv/BX-Books.csv', sep=';', error_bad_lines=False, encoding="latin-1")
books.columns = ['ISBN', 'bookTitle', 'bookAuthor', 'yearOfPublication', 'publisher', 'imageUrlS', 'imageUrlM',
                 'imageUrlL']
users = pd.read_csv('recsys/csv/BX-Users.csv', sep=';', error_bad_lines=False, encoding="latin-1")
users.columns = ['userID', 'Location', 'Age']
ratings = pd.read_csv('recsys/csv/BX-Book-Ratings.csv', sep=';', error_bad_lines=False, encoding="latin-1")
ratings.columns = ['userID', 'ISBN', 'bookRating']
ratings = ratings[ratings.ISBN != '0679781587']


# books = pd.read_csv('csv/BX-Books.csv', sep=';', error_bad_lines=False, encoding="latin-1")
# books.columns = ['ISBN', 'bookTitle', 'bookAuthor', 'yearOfPublication', 'publisher', 'imageUrlS', 'imageUrlM',
#                  'imageUrlL']
# users = pd.read_csv('csv/BX-Users.csv', sep=';', error_bad_lines=False, encoding="latin-1")
# users.columns = ['userID', 'Location', 'Age']
# ratings = pd.read_csv('csv/BX-Book-Ratings.csv', sep=';', error_bad_lines=False, encoding="latin-1")
# ratings.columns = ['userID', 'ISBN', 'bookRating']


def Top_hundred(data, ID_col, User_col):
    grouped_data = data[[ID_col, User_col]].groupby(ID_col).count().reset_index()
    grouped_data.rename(columns={User_col: "Count"}, inplace=True)
    merged_data = pd.merge(data, grouped_data.sort_values("Count", ascending=False).head(numofbooks), on=ID_col,
                           how="inner")
    return merged_data


new_ratings = Top_hundred(ratings, 'ISBN', 'userID').sort_values('Count', ascending=False)
bookDetails = pd.merge(new_ratings, books, on='ISBN', how='inner')
bookTitle = bookDetails.bookTitle.unique()
bookImage = bookDetails.imageUrlL.unique()
newer_rating = new_ratings.copy()
newer_rating['userID'] = pd.factorize(newer_rating.userID)[0]
newer_rating['ISBN'] = pd.factorize(newer_rating.ISBN)[0]
details = dict(zip(bookTitle, bookImage))


# This function is used to compute adjusted cosine similarity matrix for the books
def computeAdjCosSim(M):
    sim_matrix = np.zeros((M.shape[1], M.shape[1]))
    M_u = M.mean(axis=1)  # means

    for i in range(M.shape[1]):
        for j in range(M.shape[1]):
            if i == j:

                sim_matrix[i][j] = 1
            else:
                if i < j:

                    sum_num = sum_den1 = sum_den2 = 0
                    for k, row in M.loc[:, [i, j]].iterrows():

                        if ((M.loc[k, i] != 0) and (M.loc[k, j] != 0)):
                            num = (M[i][k] - M_u[k]) * (M[j][k] - M_u[k])
                            den1 = (M[i][k] - M_u[k]) ** 2
                            den2 = (M[j][k] - M_u[k]) ** 2

                            sum_num = sum_num + num
                            sum_den1 = sum_den1 + den1
                            sum_den2 = sum_den2 + den2

                        else:
                            continue

                    den = (sum_den1 ** 0.5) * (sum_den2 ** 0.5)
                    if den != 0:
                        sim_matrix[i][j] = sum_num / den
                    else:
                        sim_matrix[i][j] = 0


                else:
                    sim_matrix[i][j] = sim_matrix[j][i]

    return pd.DataFrame(sim_matrix)


# This function finds k most similar book to the book passed to it as an argument
def findksimilaritems_adjcos(item_id, ratings, k=k):
    sim_matrix = computeAdjCosSim(ratings)
    print(sim_matrix)
    similarities = sim_matrix[item_id - 1].sort_values(ascending=False)[:k + 1].values
    indices = sim_matrix[item_id - 1].sort_values(ascending=False)[:k + 1].index

    print('{0} most similar items for item {1}:\n'.format(k, item_id))

    for i in range(0, len(indices)):
        if indices[i] + 1 == item_id:
            continue;

        else:
            print('{0}: Item {1} :, with similarity of {2}'.format(i, indices[i] + 1, similarities[i]))

    return similarities, indices


# This function predicts the rating for specified user-item combination for adjusted cosine item-based approach
# As the adjusted cosine similarities range from -1,+1, sometimes the predicted rating can be negative or greater than max value
# Hack to deal with this: Rating is set to min if prediction is negative, Rating is set to max if prediction is above max
def predict_itembased_adjcos(user_id, item_id, ratings):
    prediction = 0

    similarities, indices = findksimilaritems_adjcos(item_id,
                                                     ratings)  # similar users based on correlation coefficients
    sum_wt = np.sum(similarities) - 1
    product = 1
    wtd_sum = 0
    for i in range(0, len(indices)):
        if indices[i] + 1 == item_id:
            continue
        else:
            product = ratings.iloc[user_id - 1, indices[i]] * (similarities[i])
            wtd_sum = wtd_sum + product
    prediction = int(round(wtd_sum / sum_wt))
    if prediction < 0:
        prediction = 1
    elif prediction > 10:
        prediction = 10
    print('\nPredicted rating for user {0} -> item {1}: {2}'.format(user_id, item_id, prediction))

    return similarities, indices, prediction


def printHello(x, y):
    matrix = x.pivot(
        index='userID',
        columns='ISBN',
        values='bookRating'
    ).fillna(0)
    similarities, indices, prediction = predict_itembased_adjcos((len(new_ratings.userID.unique()) + 1), y, matrix)
    return similarities, indices, prediction
